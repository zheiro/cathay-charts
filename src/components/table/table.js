import React, { Component } from 'react'
import rawdata from '../../assets/bardata'
import ReactTable from 'react-table'
import 'react-table/react-table.css'


const columns = [{
          Header: "行销通路",
          accessor: "行销通路"
        }, {
          Header: "回應次數",
          accessor: "回應次數"
        }, {
          Header: "行銷次數",
          accessor: "行銷次數"
        },]
        
        
export default class TableData extends Component {
  
  render () {
    return(
      <div style={{width:1200, height:600}}>
        <ReactTable
        data={rawdata}
        columns={columns}
        defaultPageSize={7}
        page={0}
        showPagination={false} 
        />
      </div>
    )
  }
}