import React, { Component } from 'react'
import Checkbox from 'material-ui/Checkbox'
import * as d3 from 'd3'
import rawdata from '../../assets/bubbledata'
import './style.css'


const items = [
  '消費相闒',
  'MCC偏好',
  '特殊標幟'
];


export default class BubbleChartWithFilter extends Component {

  constructor() {
    super()
    this.state = {
      options: ['消費相闒', 'MCC偏好', '特殊標幟'],
      data : rawdata.map(item => Object.assign({}, item))
    }
  }
  
  updateCheck(e) {
    // current array of options
    let options = this.state.options
    let index

    // check if the check box is checked or unchecked
    if (e.target.checked) {
      // add the numerical value of the checkbox to options array
      options.push(items[e.target.value])
      this.setState({ options: options })
    } else {
      // or remove the value from the unchecked checkbox from the array
      index = options.indexOf(items[e.target.value])
      options.splice(index, 1)
      this.setState({ options: options })
    }
  }
 
 
  render () {
    let width = window.innerWidth,
        height = window.innerHeight;
    
    return (
      <div>
        <div style={{width: window.innerWidth, height: window.innerHeight, flex: '1'}}>
          <div style={{display: 'flex', justifyContent: 'space-between', marginTop: '30px'}}>
            <div style={{position:'absolute', left:'12%', color : 'rgb(69, 150, 100)', fontSize: '30px', marginLeft: '60px'}}>
                線上瀏覧行為意圖
            </div>
            <div style={{position:'absolute', right:'22%', color : 'rgb(69, 150, 100)', fontSize: '30px', marginRight: '-60px'}}>
                線下消費行為意圖
            </div>
          </div>
          <div id="bubblestatic-chart" style={{position: 'absolute', left:'6%', width: width/2, height:'100%', marginTop: '40px'}}>
            {/*space for bubble chart*/}
          </div>
          
          <div style={{position:'absolute', left:'55%', marginTop: '50px'}}>
            <div style={{position:'absolute', right:'40%', display: 'flex', flexDirection: 'row', justifyContent: 'center', marginRight: '-40px'}}>     
              <Checkbox
                label="消費相闒"
                onCheck={this.updateCheck.bind(this)}
                defaultChecked
                value={0}
                style={{ whiteSpace: 'nowrap', overflow: 'hidden', width: 'auto', margin:'10px'}}
              />   
              <Checkbox
                label="MCC偏好"
                onCheck={this.updateCheck.bind(this)}
                defaultChecked
                value={1}
                style={{ whiteSpace: 'nowrap', overflow: 'hidden', width: 'auto', margin:'10px'}}
              />
              <Checkbox
                label="特殊標幟"
                onCheck={this.updateCheck.bind(this)}
                defaultChecked
                value={2}
                style={{ whiteSpace: 'nowrap', overflow: 'hidden', width: 'auto', margin:'10px'}}
              />
            </div>
            <div id="bubblefilter-chart" style = {{marginTop: '0px', width: width/2}}>
              {/*space for bubble chart*/}
            </div>
          </div>
        </div>
      </div>  
    )
  }

  createBubbleChartStatic(data) {
    //   D3 code below 
    
    var diameter = 550; //max size of the bubbles
    
    var bubbleStatic = d3.layout.pack()
      .sort(null)
      .size([diameter, diameter])
      .padding(1.5);

    var svg = d3.select("#bubblestatic-chart")
      .classed("svg-container", true) //container class to make it responsive
      .append("svg")
      .attr("width", diameter)
      .attr("height", diameter)
      .attr("class", "bubble-static")
      //responsive SVG needs these 2 attributes and no width and height attr
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", "0 0 600 400")
      //class to make it responsive
      .classed("svg-content-responsive", true); 
      
    //convert numerical values from strings to numbers
    var data = rawdata.map(function(d){ d.value = +d["Payment.Frequency"]; return d; })
    .filter(function(d){ return d["TransactionType"] !== "Offline"; });
    
    data = data.sort(function (a, b) {
            return -(a["Payment.Frequency"] - b["Payment.Frequency"]);
        })
        
    //bubbles needs very specific format, convert data to this.
    var nodes = bubbleStatic.nodes({children:data}).filter(function(d) { return !d.children; });

    //setup the chart
    var bubblesStatic = svg.append("g")
        .attr("transform", "translate(0,0)")
        .selectAll(".bubble")
        .data(nodes)
        .enter();
        
    var tooltip = d3.select("#bubblestatic-chart")
        .append("div")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("visibility", "hidden")
        .style("color", "white")
        .style("padding", "8px")
        .style("background-color", "rgba(0, 0, 0, 0.75)")
        .style("border-radius", "6px")
        .style("font", "24px sans-serif")
        .text("tooltip");        
    
    var color = d3.scale.linear()
                .domain([0, d3.max(data, function (d) {
                return d["Payment.Frequency"];
                })])
                .interpolate(d3.interpolateRgb)
                .range(["#ff0000", "#11a847"]);
    
    //create the bubbles
    function changecolor(transition, fill) {
      transition
          .style("fill", fill);
    }
    
    var ramp = d3.scale.linear().domain([d3.min(data, function(d) {
            return +d["value"];
        }), d3.max(data, function(d) {
            return +d["value"];
        })]).interpolate(d3.interpolateRgb).range([d3.rgb("#a8e0bc"), d3.rgb("#11a847")]);
    
    
    bubblesStatic.append("circle")
        .attr("r", function(d){ return 0; })
        .attr("cx", function(d){ return d3.max(d.x - 100, 0); })
        .attr("cy", function(d){ return d3.max(d.y - 100, 0); })
        .attr("class", "circle-static")
        .style("fill", "#a8e0bc")
        .on("mouseover", function(d) {
              tooltip.style("visibility", "visible");
              d3.select(this).style('stroke', 'black');
        })
        .on("mousemove", function(d) {
            return tooltip.html(d["Tags"] + ": " + d["Payment.Frequency"])  .style("left", d3.select(this).attr("cx") + 100 + "px")     
              .style("top", d3.select(this).attr("cy") - 50  + "px");
        })
        .on("mouseout", function(){
             tooltip.style("visibility", "hidden")
             d3.select(this).style("stroke", "none");});

    d3.selectAll(".circle-static")
      .transition()
          .attr("r", function(d){ return d.r; })
          .attr("cx", function(d){ return d.x; })
          .attr("cy", function(d){ return d.y; })
          .ease('cubic-in-out')  
          .delay(function(d, i) {
                return i * (1000/8);
            })
          //.duration(500)
          .call(changecolor, function(d){
          return ramp(d["Payment.Frequency"]); });
          
    //format the text for each bubble
    bubblesStatic.append("text")
      .transition()    
        .ease('cubic-in-out')  
        .delay(function(d, i) {
                return i * (1000/8);
            })
        //.duration(1000)
        .attr("x", function(d){ return d.x; })
        .attr("y", function(d){ return d.y + 5; })
        .attr("text-anchor", "middle")
        .text(function(d){ return d.Tags.substring(0, d.r / 6); })
        .style({
            "fill": "#ffffff", 
            "font-family":"Helvetica Neue, Helvetica, Arial, san-serif"
        })
  }

  createBubbleFilteredChart (data) {
    //D3 code below 
    
    var diameter = 550; //max size of the bubbles
    
    var bubble = d3.layout.pack()
      .sort(null)
      .size([diameter, diameter])
      .value(function(d) { return d["Payment.Frequency"]; })
      .padding(1.5);

    var svg = d3.select("#bubblefilter-chart")
      .classed("svg-container", true) //container class to make it responsive
      .append("svg")
      .attr("width", diameter)
      .attr("height", diameter)
      .attr("class", "bubble")
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", "0 0 600 400")
      //class to make it responsive
      .classed("svg-content-responsive", true); 
    
                          
    //bubbles needs very specific format, convert data to this.
    var nodes = bubble.nodes({children:data}).filter(function(d) { return !d.children; });

    //setup the chart
    var bubbles = svg.append("g")
        .attr("transform", "translate(0,0)")
        .selectAll(".bubble")
        .data(nodes)
        .enter();
        
    var tooltip = d3.select("#bubblefilter-chart")
        .append("div")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("visibility", "hidden")
        .style("color", "white")
        .style("padding", "8px")
        .style("background-color", "rgba(0, 0, 0, 0.75)")
        .style("border-radius", "6px")
        .style("font", "18px sans-serif")
        .text("tooltip");        
    
    var ramp = d3.scale.linear().domain([d3.min(data, function(d) {
            return +d["Payment.Frequency"];
        }), d3.max(data, function(d) {
            return +d["Payment.Frequency"];
        })]).interpolate(d3.interpolateRgb).range([d3.rgb("#a8e0bc"), d3.rgb("#11a847")]);
        
    //create the bubbles
    function changecolor(transition, fill) {
      transition
          .style("fill", fill);
    }

    bubbles.append("circle")
        .attr("r", function(d){ return 0; })
        .attr("cx", function(d){ return d3.max(d.x - 100, 0); })
        .attr("cy", function(d){ return d3.max(d.y - 100, 0); })
        .attr("class", "circle-filter")
        .style("fill", "#e0ffeb")
        .on("mouseover", function(d) {
              tooltip.style("visibility", "visible");
              d3.select(this).style('stroke', 'black');
        })
        .on("mousemove", function(d) {
            return tooltip.html(d["Tags"] + ": " + d["Payment.Frequency"])  .style("left", d3.select(this).attr("cx") - 10 + "px")     
              .style("top", d3.select(this).attr("cy") - 10  + "px");
        })
        .on("mouseout", function(){
             tooltip.style("visibility", "hidden")
             d3.select(this).style("stroke", "none");});;
        /*.transition()
          .ease('cubic-in-out')  
          .delay(500)
          .duration(500)
          .attr('r', function(d){ return d.r + 5; })
          .interrupt()*/
        
        
    d3.selectAll(".circle-filter")
      .transition()
          .attr("r", function(d){ return d.r; })
          .attr("cx", function(d){ return d.x; })
          .attr("cy", function(d){ return d.y; })
          .ease('cubic-in-out')  
          .delay(function(d, i) {
                return i * (1000/8);
            })
          //.duration(500)
          .call(changecolor, function(d){
          return ramp(d["Payment.Frequency"]); });

    //format the text for each bubble
    bubbles.append("text")
      .transition()    
        .ease('elastic')  
        .ease('cubic-in-out')  
        .delay(function(d, i) {
                return i * (1000/7);
            })
        //.duration(1000)
        .attr("x", function(d){ return d.x; })
        .attr("y", function(d){ return d.y + 5; })
        .attr("text-anchor", "middle")
        .style("pointer-events", "none")
        .text(function(d){ return d.Tags.substring(0, d.r / 6); })
        .style({
            "fill": "#000000", 
            "font-family":"Helvetica Neue, Helvetica, Arial, san-serif"
        });
        
  }

  componentDidMount () {
    const realData = rawdata
    let data = realData.map(item => Object.assign({}, item));
    let options = this.state.options
    
    data = data.map(function(d){ d["Payment.Frequency"] = +d["Payment.Frequency"]; return d; })
          .filter(function(d){ return d["TransactionType"] !== "Online"; })
          
    if (options && options.length) {
      data = data.filter(function(v) {
        return options.indexOf(v["Tag.Type"]) > -1;
      });
    } 
      
    data = data.reduce(function(previous, current) {
        if (!(current.Tags in previous)) {
            previous.__array.push(previous[current.Tags] = current);
        }
        else {
            previous[current.Tags]["Payment.Frequency"] += current["Payment.Frequency"]
        }
        return previous;
      }, {__array:[]}).__array
      .sort(function (a, b) {
        return -(a["Payment.Frequency"] - b["Payment.Frequency"]);
      })

    this.createBubbleChartStatic(rawdata);
    this.createBubbleFilteredChart(data);
  }

  componentWillUnmount() {
    d3.select("#bubblestatic-chart").select("svg").remove();
    d3.selectAll("#bubblefilter-chart").select("svg").remove();
  }
  
  componentWillUpdate() {
    d3.selectAll("#bubblefilter-chart").select("svg").remove();
  }
  

  componentDidUpdate() {
    const realData = rawdata
    let data = realData.map(item => Object.assign({}, item));
    let options = this.state.options
    
    data = data.map(function(d){ d["Payment.Frequency"] = +d["Payment.Frequency"]; return d; })
          .filter(function(d){ return d["TransactionType"] !== "Online"; })

    if (options && options.length) {
      data = data.filter(function(v) {
        return options.indexOf(v["Tag.Type"]) > -1;
      });
    }

      
    data = data.reduce(function(previous, current) {
        if (!(current.Tags in previous)) {
            previous.__array.push(previous[current.Tags] = current);
        }
        else {
            previous[current.Tags]["Payment.Frequency"] += current["Payment.Frequency"];
        }
        return previous;
      }, {__array:[]}).__array
      .sort(function (a, b) {
        return -(a["Payment.Frequency"] - b["Payment.Frequency"]);
      })

    this.createBubbleFilteredChart(data);
  }
  
}

