import React, { Component } from 'react'
import * as d3 from 'd3'
import ReactTable from 'react-table'
import rawData from '../../assets/bardata'
import 'react-table/react-table.css'


const columns = [
    {
        Header: () => <div style={{color: "#11a847", fontWeight: "bold"}}> 行销通路 </div>,
        accessor: "行销通路",
        Cell: row => (
            <div
                style={{
                    marginLeft: '20px'
                }}
            >
                {row.value}
            </div>
        )
    },
    {
        Header: () => <div style={{color: "#11a847", fontWeight: "bold"}}> 百分比 </div>,
        accessor: "百分比",
        Cell: row => (
            <div
                style={{
                    textAlign: 'right',
                    marginRight: '20px'
                }}
            >
                {row.value}
            </div>
        )
    },
    {
        Header: () => <div style={{color: "#11a847", fontWeight: "bold"}}> 回應次數 </div>,
        accessor: "回應次數",
        Cell: row => (
            <div
                style={{
                    textAlign: 'right',
                    marginRight: '20px'
                }}
            >
                {row.value}
            </div>
        )
    },
    {
        Header: () => <div style={{color: "#11a847", fontWeight: "bold"}}> 行銷次數 </div>,
        accessor: "行銷次數",
        Cell: row => (
            <div
                style={{
                    textAlign: 'right',
                    marginRight: '20px'
                }}
            >
                {row.value}
            </div>
        )
    }
];


export default class RadialWithBarChart extends Component {
    constructor() {
        super();
        this.state = {
            datapoint : [],
            has_loaded : false,
            current_percent : 0
        }
    }

    render () {
        let width = window.innerWidth,
            height = window.innerHeight - 100;

        return (
            <div>
                <div style={{width: width, height: height/2}}>
                    <div style={{position:'absolute', left:'0%', width:width/2, height:height/2, left: '2%'}} id="radial-chart">
                    </div>
                    <div style={{position:'absolute', right:'0%', width: 5* width/8, height:height/2}} id="bar-chart">
                    </div>
                </div>
                <div>
                    <ReactTable
                        className="-striped -highlight"
                        data={rawData}
                        columns={columns}
                        defaultPageSize={7}
                        page={0}
                        showPagination={false}
                    />
                </div>
            </div>
        )
    }

    buildBar () {
        //   D3 code below
        let self = this;

        let margin = {
            top: 15,
            right: 60,
            bottom: 15,
            left: 120
        };

        let width = d3.select("#bar-chart").node().getBoundingClientRect().width - margin.left - margin.right,
            height = d3.select("#bar-chart").node().getBoundingClientRect().height - margin.top - margin.bottom;

        let svg = d3.select("#bar-chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .attr("class", "bar")
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");


        let tooltip = d3.select("body")
            .append("div")
            .attr("class", "tooltip")
            .style("position", "absolute")
            .style("z-index", "10")
            .style("visibility", "hidden")
            .style("color", "white")
            .style("padding", "8px")
            .style("background-color", "rgba(0, 0, 0, 0.75)")
            .style("border-radius", "6px")
            .style("font", "18px sans-serif")
            .text("tooltip");

        let data = rawData;

        data.forEach(function(d) {
            d["行销通路"] = d["行销通路"];
            d["百分比"] = +d["百分比"];
        });

        data = data.sort(function (a, b) {
            return d3.ascending(a["百分比"], b["百分比"]);
        });

        let x = d3.scale.linear()
            .range([0, width])
            .domain([0, d3.max(data, function (d) {
                return d["百分比"];
            })]);

        let y = d3.scale.ordinal()
            .rangeRoundBands([height, 0], .1)
            .domain(data.map(function(d) { return d["行销通路"]; }));

        let yAxis = d3.svg.axis()
            .scale(y)
            .tickSize(15)
            .orient("left");

        let xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(0)
            .orient("bottom");

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis);

        svg.append("g")
            .attr("transform", "translate(0,470)")
            .attr("class", "x axis")
            .call(xAxis);

        let bars = svg.selectAll(".bar")
            .data(data)
            .enter()
            .append("g");

        //append rects
        if(self.state.has_loaded !== true) {
            bars.append("rect")
                .attr("class", "bar")
                .attr("x", 0)
                .attr("fill", function(d) {
                    if(self.state.datapoint.indexOf(d["行销通路"]) > -1) {
                        return "#15D859";
                    } else {
                        return "#11a847"
                    }
                })
                .attr("width", 0)
                .attr("y", function (d) {
                    return y(d["行销通路"]);
                })
                .attr("height", y.rangeBand())
                .transition()
                .duration(1000)
                .delay(function(d, i) {
                    return i * 100;
                })
                .attr("width", function (d) {
                    return x(d["百分比"]);
                });
        } else {
            bars.append("rect")
                .attr("class", "bar")
                .attr("x", 0)
                .attr("fill", function(d) {
                    if(self.state.datapoint.indexOf(d["行销通路"]) > -1) {
                        return "#15D859";
                    } else {
                        return "#11a847"
                    }
                })
                .attr("width", 0)
                .attr("y", function (d) {
                    return y(d["行销通路"]);
                })
                .attr("height", y.rangeBand())
                .attr("width", function (d) {
                    return x(d["百分比"]);
                });
        }

        bars.on("mouseover", function(d) {
            tooltip.html(
                d["行销通路"] + ": " + d["百分比"] + "<br />" +
                "回應次數" + ": " + d["回應次數"] + "<br />" +
                "行銷次數" + ": " + d["行銷次數"]
            );
            tooltip.style("visibility", "visible");
            d3.select(this)
                .style("stroke", "red")
                .select("rect")
                .attr("fill", "#15D859");
        })
            .on("mousemove", function() {
                return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
            })
            .on("mouseout", function() {
                tooltip.style("visibility", "hidden");
                d3.select(this)
                    .style("stroke", "none")
                    .select("rect")
                    .attr("fill", "#11a847");

                if (self.state.datapoint.indexOf(d3.select(this).datum()['行销通路']) > -1) {
                    d3.select(this).select("rect").attr("fill", "#15D859");
                }
            })
            .on('click', function() {
                d3.selectAll(".bar").attr("fill", "#11a847");
                d3.select(this).select("rect").attr("fill", "#15D859");
                let filtered = d3.select(this).datum()["行销通路"];
                let data_points = self.state.datapoint;

                if (data_points.indexOf(filtered) === -1) {
                    data_points.push(filtered);
                    self.setState({
                        datapoint: data_points,
                        has_loaded: true
                    });
                }
                else {
                    data_points.pop(filtered);
                    self.setState({
                        datapoint: data_points,
                        has_loaded: true
                    });
                }
                d3.event.stopPropagation();     // Without this, the event below will also trigger.
            });

        d3.select("#bar-chart")
            .on("click", function(d) {
                self.setState({
                    datapoint: [],
                    has_loaded: true
                })
            })
    }

    buildRadial() {
        // Constants and d3 scales and format.
        let width = d3.select("#radial-chart").node().getBoundingClientRect().width,       // Width of the graph.
            height = d3.select("#radial-chart").node().getBoundingClientRect().height,    // Height of the graph.
            radius = Math.min(width, height) / 2.5,   // Radius of the graph.
            pie = d3.layout.pie().sort(null)
                .padAngle(0.01),                    // Pie scale.
            format = d3.format(".2%"),              // Format for the text.
            colors = ["#2a3a46", "#11a847"];        // Colors for the chart.

        // Computation for the percentage to be shown in the chart.
        let data = rawData;

        // filter for the highlighted bar
        if (this.state.datapoint.length !== 0) {
            let datapoint = this.state.datapoint;
            data = data.filter(function(v) {
                if (datapoint.indexOf(v["行销通路"]) >= 0) {
                    return v;
                }
            });
        }

        data.forEach(function(d) {
            d["行銷次數"] = +d["行銷次數"];
            d["回應次數"] = +d["回應次數"];
        });

        let total_sent = 0;
        let total_responded = 0;

        data.forEach(function(d) {
            total_sent += d["行銷次數"];
            total_responded += d["回應次數"];
        });

        // The percent response rate is a number between 0 to 100.
        let percent_responded = total_responded / total_sent * 100;

        // Dataset to be used for the chart.
        let dataset = {
            lower: [0, 100],
            upper: [percent_responded, 100-percent_responded]
        };

        // Arc used for the chart.
        let arc = d3.svg.arc()
            .innerRadius(radius*0.8)
            .outerRadius(radius);

        // Arc used for the pop-up effect.
        let arcOver = d3.svg.arc()
            .innerRadius(radius*0.825)
            .outerRadius(radius*1.05);

        // Tooltip div.
        let tooltip = d3.select("body")
            .append("div")
            .attr("class", "tooltip")
            .style("position", "absolute")
            .style("z-index", "10")
            .style("visibility", "hidden")
            .style("color", "white")
            .style("padding", "8px")
            .style("background-color", "rgba(0, 0, 0, 0.75)")
            .style("border-radius", "6px")
            .style("font", "18px sans-serif")
            .text("tooltip");

        // Bind the whole object to the 'self' variable.
        let self = this;

        // SVG for the chart.
        let svg = d3.select("#radial-chart")
            .append("svg")
            .attr({
                width: width,
                height: height
            })
            .append("g")
            .attr("transform", "translate(" + width/4 + "," + height/2 + ")");

        // The actual arcs.
        let path = svg.selectAll("path")
            .data(pie(dataset.lower))
            .enter()
            .append("path")
            .attr("fill", function(d, i){
                return colors[1-i]
            })
            .attr("d", arc)
            .each(function(d){
                this._current = d;
            });

        path.on("mouseover", function(d, i) {
            let data = self.state.datapoint;
            if(i === 0){
                if (data.length === 0){
                    tooltip.html("Total");
                }
                else {
                    tooltip.html(data.join(", "));
                }
                tooltip.style("visibility", "visible");
                d3.select(this).transition()
                    .attr("d", arcOver);
            }
        })
            .on("mousemove", function(){
                return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
            })
            .on("mouseout", function() {
                tooltip.style("visibility", "hidden");
                d3.select(this).transition()
                    .attr("d", arc);
            })
        ;

        // Text in the middle for the percentage.
        svg.append("text")
            .attr("text-anchor", "middle")
            .attr("dy", "-0.5em")
            .style({
                fill: 'black',
                'font-size': '30px'
            })
            .text("回鹰率");
            
        // Text for the numbers on the side.
        svg.append("text")
            .attr("text-anchor", "middle")
            .attr("dy", "-1em")
            .attr("dx", "9em")
            .style({
                fill: 'black',
                'font-size': '20px'
            })
            .text("回應次數:");
        
        // Text for the numbers on the side.
        svg.append("text")
            .attr("text-anchor", "middle")
            .attr("dy", "1em")
            .attr("dx", "9em")
            .style({
                fill: 'black',
                'font-size': '20px'
            })
            .text("行銷次數:");
        
        // Percentage at the middle.
        let text = svg.append("text")
            .attr("text-anchor", "middle")
            .attr("dy", "0.75em")
            .style({
                fill: 'black',
                'font-size': '35px'
            });
        
        // Number for 回應次數
        let text2 = svg.append("text")
            .attr("text-anchor", "middle")
            .attr("dy", "-1em")
            .attr("dx", "12.5em")
            .style({
                fill: "#11a847",
                'font-size': '20px',
                'font-weight': '500'
            })
            .text(total_responded + '次');
            
        // Number for 行銷次數
        let text3 = svg.append("text")
            .attr("text-anchor", "middle")
            .attr("dy", "1em")
            .attr("dx", "12.5em")
            .style({
                fill: "#11a847",
                'font-size': '20px',
                'font-weight': '500'
            })
            .text(total_sent + '次');
            
        let timeout = setTimeout(function() {
            clearTimeout(timeout);
            path = path.data(pie(dataset.upper));
            path.transition()
                .duration(1000)
                .attrTween("d", function(a) {
                    let i = d3.interpolate(this._current, a);
                    let i2 = d3.interpolate(0, percent_responded);
                    this._current = i(0);
                    return function(t) {
                        text.html(format(i2(t)/100));
                        return arc(i(t));
                    }
                })
        }, 0);
    }

    componentDidMount () {
        this.buildBar();
        this.buildRadial();
    }

    componentWillUnmount() {
        d3.select("#bar-chart").select("svg").remove();
        d3.selectAll("#radial-chart").select("svg").remove();
    }

    componentWillUpdate() {
        d3.select("#bar-chart").select("svg").remove();
        d3.selectAll(".tooltip").remove();
        d3.selectAll("#radial-chart").select("svg").remove();
    }

    componentDidUpdate() {
        this.buildBar();
        this.buildRadial();
    }
}
