import React, { Component } from 'react';
import './timeline-style.css'
import * as d3 from 'd3';
import timelineData from '../../assets/timelinedata';

export default class TimelineChart extends Component {
  render() {
    return <div id="timeline-charts"></div>
  }

  componentDidMount () {
    function addAxesAndLegend (svg, xAxis, yAxis, margin, chartWidth, chartHeight) {
      var legendWidth  = 252,
          legendHeight = 100;

      // clipping to make sure nothing appears behind legend
      svg.append('clipPath')
        // .attr('id', 'axes-clip')
        .append('polygon')
          .attr('points', (-margin.left)                 + ',' + (-margin.top)                 + ' ' +
                          (chartWidth - legendWidth - 1) + ',' + (-margin.top)                 + ' ' +
                          (chartWidth - legendWidth - 1) + ',' + legendHeight                  + ' ' +
                          (chartWidth + margin.right)    + ',' + legendHeight                  + ' ' +
                          (chartWidth + margin.right)    + ',' + (chartHeight + margin.bottom) + ' ' +
                          (-margin.left)                 + ',' + (chartHeight + margin.bottom));

      var axes = svg.append('g')
        .attr('clip-path', 'url(#axes-clip)');

      axes.append('g')
        .attr('class', 'x axis')
        .attr('transform', 'translate(0,' + chartHeight + ')')
        .call(xAxis);

      axes.append('g')
        .attr('class', 'y axis')
        .call(yAxis)
        .append('text')
          .attr('transform', 'rotate(-90)')
          .attr('y', 6)
          .attr('dy', '.71em')
          .style('text-anchor', 'end')
          .text('Time (Hour)');


    //build the legend
      var legend = svg.append('g')
        .attr('class', 'legend')
        .attr('transform', 'translate(' + (chartWidth - legendWidth) + ', 0)');

      legend.append('rect')
        .attr('class', 'legend-bg')
        .attr('width',  legendWidth)
        .attr('height', legendHeight);

      legend.append('rect')
        .attr('class', 'atm-rect')
        .attr('width',  40)
        .attr('height', 30)
        .attr('x', 5)
        .attr('y', 10);

      legend.append('svg:image')
        .attr('class', 'atm-image')
        .attr('x', 10)
        .attr('y', 10)
        .attr('width', 30)
        .attr('height', 30)
        .attr("xlink:href", require('./atm.png'));

      legend.append('text')
        .attr('class', 'atm-legend')
        .attr('x', 50)
        .attr('y', 30)
        .text('ATM');

      legend.append('rect')
        .attr('class', '客服進線-rect')
        .attr('width',  40)
        .attr('height', 30)
        .attr('x', 5)
        .attr('y', 55);

      legend.append('svg:image')
        .attr('class', '客服進線-image')
        .attr('x', 10)
        .attr('y', 55)
        .attr('width', 30)
        .attr('height', 30)
        .attr("xlink:href", require('./person.png'));

      legend.append('text')
        .attr('class', '客服進線-legend')
        .attr('x', 50)
        .attr('y', 75)
        .text('客服進線');

      legend.append('rect')
        .attr('class', '網銀-rect')
        .attr('width',  40)
        .attr('height', 30)
        .attr('x', 130)
        .attr('y', 10);

      legend.append('svg:image')
        .attr('class', '網銀-image')
        .attr('x', 135)
        .attr('y', 10)
        .attr('width', 30)
        .attr('height', 30)
        .attr("xlink:href", require('./phone.png'));

      legend.append('text')
        .attr('class', '網銀-legend')
        .attr('x', 175)
        .attr('y', 30)
        .text('網銀');

      legend.append('rect')
        .attr('class', '信用卡-rect')
        .attr('width',  40)
        .attr('height', 30)
        .attr('x', 130)
        .attr('y', 55);

      legend.append('svg:image')
        .attr('class', '信用卡-image')
        .attr('x', 135)
        .attr('y', 55)
        .attr('width', 30)
        .attr('height', 30)
        .attr("xlink:href", require('./mail.png'));

      legend.append('text')
        .attr('class', '信用卡-legend')
        .attr('x', 175)
        .attr('y', 75)
        .text('信用卡');
      
    }

    function drawPaths (svg, time, x, y, svgWidth, svgHeight) {
      // var upperOuterArea = d3.svg.area()
      //   .interpolate('basis')
      //   .x (function (d) { return x(d.time) || 1; })
      //   .y0(function (d) { return y(d.pct95); })
      //   .y1(function (d) { return y(d.pct75); });

      // var upperInnerArea = d3.svg.area()
      //   .interpolate('basis')
      //   .x (function (d) { return x(d.time) || 1; })
      //   .y0(function (d) { return y(d.pct75); })
      //   .y1(function (d) { return y(d.pct50); });

      // var medianLine = d3.svg.line()
      //   .interpolate('linear')
      //   .x(function (d) { return x(d.time); })
      //   .y(function (d) { return y(d.pct50); });

      // var lowerInnerArea = d3.svg.area()
      //   .interpolate('basis')
      //   .x (function (d) { return x(d.time) || 1; })
      //   .y0(function (d) { return y(d.pct50); })
      //   .y1(function (d) { return y(d.pct25); });

      // var lowerOuterArea = d3.svg.area()
      //   .interpolate('basis')
      //   .x (function (d) { return x(d.time) || 1; })
      //   .y0(function (d) { return y(d.pct25); })
      //   .y1(function (d) { return y(d.pct05); });

      svg.datum(time);

      // svg.append('path')
      //   .attr('class', 'area upper outer')
      //   .attr('d', upperOuterArea)
      //   .attr('clip-path', 'url(#rect-clip)');

      // svg.append('path')
      //   .attr('class', 'area lower outer')
      //   .attr('d', lowerOuterArea)
      //   .attr('clip-path', 'url(#rect-clip)');

      // svg.append('path')
      //   .attr('class', 'area upper inner')
      //   .attr('d', upperInnerArea)
      //   .attr('clip-path', 'url(#rect-clip)');

      // svg.append('path')
      //   .attr('class', 'area lower inner')
      //   .attr('d', lowerInnerArea)
      //   .attr('clip-path', 'url(#rect-clip)');

      //adjust arrow size
      let arrowWidth = svgWidth;
      let arrowHeight = svgHeight + 120;

      var arrow = svg.append('path')
        .attr('class', 'median-line')
        .attr('opacity', 0);
        // .attr('clip-path', 'url(#rect-clip)');

      d3.select('.median-line')
        .transition()
        .duration(1000)
        .attr('d', "M 0 "+(arrowHeight-280)+" L "+(arrowWidth-100)+" "+(arrowHeight-280)+" L "+(arrowWidth-100)+" "+(arrowHeight-295)+" L "+(arrowWidth-62)+" "+(arrowHeight-255)+" L "+(arrowWidth-100)+" "+(arrowHeight-215)+" L "+(arrowWidth-100)+" "+(arrowHeight-230)+" L 0 "+(arrowHeight-230)+" L 0 "+(arrowHeight-280))
        .attr('opacity', 1);
    }

    function addMarker (marker, svg, chartHeight, x, y) {
      //setting the position of the marker
      // let yPosEndMarker

      // switch (marker.type) {
      //   case 'ATM' : yPosEndMarker = 175
      //     break;
      //   case '客服進線' : yPosEndMarker = 250
      //     break;
      //   case '信用卡' : yPosEndMarker = 325
      //     break;
      //   case '網銀' : yPosEndMarker = 400
      //     break;
      // }

      let eventPosMarker = 35;
      //setting the position of the event label
      switch (marker.event) {
        case 'COSTCO自動扣缴會費' : eventPosMarker = 100
          break;
        case '登入問題' : eventPosMarker = 55
          break;
        case '企業行動密碼 (OTP)' : eventPosMarker = 100
          break;
      }

      let imageMarker

      //setting the image markers
      switch (marker.type) {
        case 'ATM' : imageMarker = require('./atm.png');
          break;
        case '網銀' : imageMarker = require('./phone.png');
          break;
        case '客服進線' : imageMarker = require('./person.png');
          break;
        case '信用卡' : imageMarker = require('./mail.png');
          break;
      }

      var radius = 8,
          xPos = x(marker.date) - radius - 3,
          yPosStart = chartHeight - radius - 3,
          yPosEnd = y(marker.time.getHours()) + radius - 20;

      var markerG = svg.append('g')
        .attr('class', 'marker '+marker.type.toLowerCase())
        .attr('transform', 'translate(' + xPos + ', ' + yPosStart + ')')
        .attr('opacity', 0);

      markerG.transition()
        .duration(1000)
        .attr('transform', 'translate(' + xPos + ', ' + yPosEnd + ')')
        .attr('opacity', 1);

      markerG.append('path')
        .attr('d', 'M' + radius + ',' + (chartHeight-yPosStart) + 'L' + radius + ',' + (chartHeight-yPosStart))
        .transition()
          .duration(1000)
          .attr('d', 'M' + radius + ',' + (chartHeight-yPosEnd) + 'L' + radius + ',' + (radius*2));

      markerG.append('circle')
        .attr('class', 'marker-bg')
        .attr('cx', radius)
        .attr('cy', radius)
        .attr('r', radius);

      markerG.append('svg:image')
        .attr('class', marker.type)
        .attr('x', radius-15)
        .attr('y', radius*0.9-70)
        .attr('width', 30)
        .attr('height', 30)
        .attr("xlink:href", imageMarker);

      markerG.append('text')
        .attr('x', radius)
        .attr('y', radius*0.9-10)
        .text(marker.time.getHours()+":"+marker.time.getMinutes());
      
      markerG.append('text')
        .attr('x', radius)
        .attr('y', radius*0.9-25)
        .text(marker.date.getMonth()+"/"+marker.date.getDate());

      markerG.append('text')
        .attr('class', 'marker-event')
        .attr('x', radius+10)
        .attr('y', eventPosMarker)
        .text(marker.event);
    }

    function startTransitions (svg, chartWidth, chartHeight, rectClip, markers, x, y) {
      rectClip.transition()
        .duration(1000*markers.length)
        .attr('width', chartWidth);

      markers.forEach(function (marker, i) {
        setTimeout(function () {
          addMarker(marker, svg, chartHeight, x, y);
        }, 1000 + 500*i);
      });
    }

    function makeChart (time, markers) {
      var svgWidth  = window.innerWidth - 30,
          svgHeight = window.innerHeight - 100,
          margin = { top: 20, right: 20, bottom: 40, left: 40 },
          chartWidth  = svgWidth  - margin.left - margin.right,
          chartHeight = svgHeight - margin.top  - margin.bottom;

      var x = d3.time.scale().range([0, chartWidth])
                .domain(d3.extent(time, function (d) {return d.time; })).nice(),
          y = d3.scale.linear().range([chartHeight, 0])
                // .domain(d3.extent(markers, function (d) {return d.time.getHours(); }));
                .domain([0, 24]);

      var xAxis = d3.svg.axis().scale(x).orient('bottom')
                    .innerTickSize(-chartHeight).outerTickSize(0).tickPadding(10),
          yAxis = d3.svg.axis().scale(y).orient('left')
                    .innerTickSize(-chartWidth).outerTickSize(0).tickPadding(10);

      var svg = d3.select('#timeline-charts').append('svg')
        .attr('width',  svgWidth)
        .attr('height', svgHeight)
        .append('g')
          .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

      // clipping to start chart hidden and slide it in later
      var rectClip = svg.append('clipPath')
        .attr('id', 'rect-clip')
        .append('rect')
          .attr('width', 0)
          .attr('height', chartHeight);

      addAxesAndLegend(svg, xAxis, yAxis, margin, chartWidth, chartHeight);
      drawPaths(svg, time, x, y, svgWidth, svgHeight);
      startTransitions(svg, chartWidth, chartHeight, rectClip, markers, x, y);
    }

    var parseTime  = d3.time.format('%H:%M').parse;

    var parseDate = d3.time.format('%d-%b').parse;

    var time = timelineData.map(function (d) {
      return {
        time:  parseDate(d.Date)
      };
    });

    var markers = timelineData.map(function (marker) {
      return {
        time: parseTime(marker.Time),
        type: marker['Channel.Type'],
        event: marker.Event,
        date: parseDate(marker.Date)
      };
    });

    makeChart(time, markers);
  }

  componentWillUnmount () {
    d3.select("#timeline-charts").select("svg").remove();
  }
}