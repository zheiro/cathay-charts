const rawData = [
 {
   "Tags": "儲蓄",
   "Eng.Name": "Savings",
   "TransactionType": "Online",
   "Payment.Frequency": 20,
   "Tag.Type": ""
 },
 {
   "Tags": "商務 / 出差",
   "Eng.Name": "Business / Travel",
   "TransactionType": "Online",
   "Payment.Frequency": 16,
   "Tag.Type": ""
 },
 {
   "Tags": "有保險",
   "Eng.Name": "Insurance Payment",
   "TransactionType": "Online",
   "Payment.Frequency": 12,
   "Tag.Type": ""
 },
 {
   "Tags": "繳費",
   "Eng.Name": "Payment",
   "TransactionType": "Online",
   "Payment.Frequency": 20,
   "Tag.Type": ""
 },
 {
   "Tags": "有小孩",
   "Eng.Name": "Have Children",
   "TransactionType": "Online",
   "Payment.Frequency": 4,
   "Tag.Type": ""
 },
 {
   "Tags": "美食",
   "Eng.Name": "Food",
   "TransactionType": "Online",
   "Payment.Frequency": 6,
   "Tag.Type": ""
 },
 {
   "Tags": "計程車",
   "Eng.Name": "Taxi",
   "TransactionType": "Online",
   "Payment.Frequency": 6,
   "Tag.Type": ""
 },
 {
   "Tags": "退休",
   "Eng.Name": "Retirement",
   "TransactionType": "Online",
   "Payment.Frequency": 2,
   "Tag.Type": ""
 },
 {
   "Tags": "悠遊卡",
   "Eng.Name": "Easy Card",
   "TransactionType": "Online",
   "Payment.Frequency": 8,
   "Tag.Type": ""
 },
 {
   "Tags": "愛心捐款",
   "Eng.Name": "Love Donations",
   "TransactionType": "Online",
   "Payment.Frequency": 14,
   "Tag.Type": ""
 },
 {
   "Tags": "旅遊",
   "Eng.Name": "Tourism",
   "TransactionType": "Online",
   "Payment.Frequency": 6,
   "Tag.Type": ""
 },
 {
   "Tags": "国内旅遊",
   "Eng.Name": "Domestic Tourism",
   "TransactionType": "Online",
   "Payment.Frequency": 8,
   "Tag.Type": ""
 },
 {
   "Tags": "高教",
   "Eng.Name": "Higher Education",
   "TransactionType": "Online",
   "Payment.Frequency": 4,
   "Tag.Type": ""
 },
 {
   "Tags": "换匯",
   "Eng.Name": "Exchange",
   "TransactionType": "Online",
   "Payment.Frequency": 6,
   "Tag.Type": ""
 },
 {
   "Tags": "消費分期",
   "Eng.Name": "Consumption Staging",
   "TransactionType": "Online",
   "Payment.Frequency": 10,
   "Tag.Type": ""
 },
 {
   "Tags": "—般購物",
   "Eng.Name": "Shopping",
   "TransactionType": "Online",
   "Payment.Frequency": 12,
   "Tag.Type": ""
 },
 {
   "Tags": "KKBOX",
   "Eng.Name": "KKBOX",
   "TransactionType": "Offline",
   "Payment.Frequency": 20,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "eTag",
   "Eng.Name": "eTag",
   "TransactionType": "Offline",
   "Payment.Frequency": 17,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "TRAVEL",
   "Eng.Name": "TRAVEL",
   "TransactionType": "Offline",
   "Payment.Frequency": 7,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "EDUCATION",
   "Eng.Name": "EDUCATION",
   "TransactionType": "Offline",
   "Payment.Frequency": 2,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "Formosa",
   "Eng.Name": "Formosa",
   "TransactionType": "Offline",
   "Payment.Frequency": 20,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "LOAN",
   "Eng.Name": "LOAN",
   "TransactionType": "Offline",
   "Payment.Frequency": 12,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "EvaAir",
   "Eng.Name": "EvaAir",
   "TransactionType": "Offline",
   "Payment.Frequency": 3,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "NFC",
   "Eng.Name": "NFC",
   "TransactionType": "Offline",
   "Payment.Frequency": 10,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "3C",
   "Eng.Name": "3C",
   "TransactionType": "Offline",
   "Payment.Frequency": 11,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "ART",
   "Eng.Name": "ART",
   "TransactionType": "Offline",
   "Payment.Frequency": 3,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "FOOD",
   "Eng.Name": "FOOD",
   "TransactionType": "Offline",
   "Payment.Frequency": 14,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "YOYO",
   "Eng.Name": "YOYO",
   "TransactionType": "Offline",
   "Payment.Frequency": 2,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "MEDICAL",
   "Eng.Name": "MEDICAL",
   "TransactionType": "Offline",
   "Payment.Frequency": 9,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "海外消費",
   "Eng.Name": "Overseas Consumers",
   "TransactionType": "Offline",
   "Payment.Frequency": 14,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "有超跑",
   "Eng.Name": "With or Have Luxury Car",
   "TransactionType": "Offline",
   "Payment.Frequency": 12,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "有車",
   "Eng.Name": "With or Have Car",
   "TransactionType": "Offline",
   "Payment.Frequency": 7,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "中油加油消費",
   "Eng.Name": "PetroChina Fuel Consumers",
   "TransactionType": "Offline",
   "Payment.Frequency": 8,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "加油消費",
   "Eng.Name": "Fuel Consumers",
   "TransactionType": "Offline",
   "Payment.Frequency": 4,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "捷運通勤族",
   "Eng.Name": "Frequent Train or Subway Commuter",
   "TransactionType": "Offline",
   "Payment.Frequency": 9,
   "Tag.Type": "MCC偏好"
 },
 {
   "Tags": "KKBOX",
   "Eng.Name": "KKBOX",
   "TransactionType": "Offline",
   "Payment.Frequency": 18,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "eTag",
   "Eng.Name": "eTag",
   "TransactionType": "Offline",
   "Payment.Frequency": 16,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "TRAVEL",
   "Eng.Name": "TRAVEL",
   "TransactionType": "Offline",
   "Payment.Frequency": 18,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "EDUCATION",
   "Eng.Name": "EDUCATION",
   "TransactionType": "Offline",
   "Payment.Frequency": 15,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "Formosa",
   "Eng.Name": "Formosa",
   "TransactionType": "Offline",
   "Payment.Frequency": 10,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "LOAN",
   "Eng.Name": "LOAN",
   "TransactionType": "Offline",
   "Payment.Frequency": 11,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "EvaAir",
   "Eng.Name": "EvaAir",
   "TransactionType": "Offline",
   "Payment.Frequency": 18,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "NFC",
   "Eng.Name": "NFC",
   "TransactionType": "Offline",
   "Payment.Frequency": 5,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "3C",
   "Eng.Name": "3C",
   "TransactionType": "Offline",
   "Payment.Frequency": 19,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "ART",
   "Eng.Name": "ART",
   "TransactionType": "Offline",
   "Payment.Frequency": 11,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "FOOD",
   "Eng.Name": "FOOD",
   "TransactionType": "Offline",
   "Payment.Frequency": 16,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "YOYO",
   "Eng.Name": "YOYO",
   "TransactionType": "Offline",
   "Payment.Frequency": 20,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "MEDICAL",
   "Eng.Name": "MEDICAL",
   "TransactionType": "Offline",
   "Payment.Frequency": 14,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "海外消費",
   "Eng.Name": "Overseas Consumers",
   "TransactionType": "Offline",
   "Payment.Frequency": 8,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "有超跑",
   "Eng.Name": "With or Have Luxury Car",
   "TransactionType": "Offline",
   "Payment.Frequency": 10,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "有車",
   "Eng.Name": "With or Have Car",
   "TransactionType": "Offline",
   "Payment.Frequency": 6,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "中油加油消費",
   "Eng.Name": "PetroChina Fuel Consumers",
   "TransactionType": "Offline",
   "Payment.Frequency": 17,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "加油消費",
   "Eng.Name": "Fuel Consumers",
   "TransactionType": "Offline",
   "Payment.Frequency": 6,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "捷運通勤族",
   "Eng.Name": "Frequent Train or Subway Commuter",
   "TransactionType": "Offline",
   "Payment.Frequency": 10,
   "Tag.Type": "消費相闒"
 },
 {
   "Tags": "KKBOX",
   "Eng.Name": "KKBOX",
   "TransactionType": "Offline",
   "Payment.Frequency": 16,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "eTag",
   "Eng.Name": "eTag",
   "TransactionType": "Offline",
   "Payment.Frequency": 12,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "TRAVEL",
   "Eng.Name": "TRAVEL",
   "TransactionType": "Offline",
   "Payment.Frequency": 4,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "EDUCATION",
   "Eng.Name": "EDUCATION",
   "TransactionType": "Offline",
   "Payment.Frequency": 11,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "Formosa",
   "Eng.Name": "Formosa",
   "TransactionType": "Offline",
   "Payment.Frequency": 9,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "LOAN",
   "Eng.Name": "LOAN",
   "TransactionType": "Offline",
   "Payment.Frequency": 11,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "EvaAir",
   "Eng.Name": "EvaAir",
   "TransactionType": "Offline",
   "Payment.Frequency": 11,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "NFC",
   "Eng.Name": "NFC",
   "TransactionType": "Offline",
   "Payment.Frequency": 18,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "3C",
   "Eng.Name": "3C",
   "TransactionType": "Offline",
   "Payment.Frequency": 12,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "ART",
   "Eng.Name": "ART",
   "TransactionType": "Offline",
   "Payment.Frequency": 1,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "FOOD",
   "Eng.Name": "FOOD",
   "TransactionType": "Offline",
   "Payment.Frequency": 20,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "YOYO",
   "Eng.Name": "YOYO",
   "TransactionType": "Offline",
   "Payment.Frequency": 15,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "MEDICAL",
   "Eng.Name": "MEDICAL",
   "TransactionType": "Offline",
   "Payment.Frequency": 17,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "海外消費",
   "Eng.Name": "Overseas Consumers",
   "TransactionType": "Offline",
   "Payment.Frequency": 20,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "有超跑",
   "Eng.Name": "With or Have Luxury Car",
   "TransactionType": "Offline",
   "Payment.Frequency": 15,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "有車",
   "Eng.Name": "With or Have Car",
   "TransactionType": "Offline",
   "Payment.Frequency": 9,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "中油加油消費",
   "Eng.Name": "PetroChina Fuel Consumers",
   "TransactionType": "Offline",
   "Payment.Frequency": 14,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "加油消費",
   "Eng.Name": "Fuel Consumers",
   "TransactionType": "Offline",
   "Payment.Frequency": 5,
   "Tag.Type": "特殊標幟"
 },
 {
   "Tags": "捷運通勤族",
   "Eng.Name": "Frequent Train or Subway Commuter",
   "TransactionType": "Offline",
   "Payment.Frequency": 8,
   "Tag.Type": "特殊標幟"
 }
]





export default rawData