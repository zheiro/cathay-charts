const timelineData =
[
  {
    "Channel.Type": "ATM",
    "Date": "5-Apr",
    "Time": "14:11",
    "Event": "提款"
  },
  {
    "Channel.Type": "客服進線",
    "Date": "10-Apr",
    "Time": "11:16",
    "Event": "COSTCO自動扣缴會費"
  },
  {
    "Channel.Type": "網銀",
    "Date": "15-Apr",
    "Time": "13:34",
    "Event": "匯款"
  },
  {
    "Channel.Type": "客服進線",
    "Date": "18-Apr",
    "Time": "21:22",
    "Event": "登入問題"
  },
  {
    "Channel.Type": "網銀",
    "Date": "20-Apr",
    "Time": "9:58",
    "Event": "匯款"
  },
  {
    "Channel.Type": "ATM",
    "Date": "24-Apr",
    "Time": "18:28",
    "Event": "刷卡"
  },
  {
    "Channel.Type": "客服進線",
    "Date": "7-May",
    "Time": "19:51",
    "Event": "企業行動密碼 (OTP)"
  },
  {
    "Channel.Type": "信用卡",
    "Date": "12-May",
    "Time": "20:23",
    "Event": "提款"
  },
  {
    "Channel.Type": "ATM",
    "Date": "22-May",
    "Time": "16:46",
    "Event": "匯款"
  },
  {
    "Channel.Type": "網銀",
    "Date": "28-May",
    "Time": "12:45",
    "Event": "定存"
  }
]

export default timelineData