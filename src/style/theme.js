import { getMuiTheme, colors } from 'material-ui/styles';

export default getMuiTheme({
  fontFamily: '"Open Sans", sans-serif',
  palette: {
    primary1Color: '#459664',
  }
})