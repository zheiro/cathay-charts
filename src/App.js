import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import theme from './style/theme';

import { Tabs, Tab } from 'material-ui/Tabs';
import { MuiThemeProvider } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';

import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom'

import BubbleWithFilterChart from './components/bubblewithfilter/BubbleWithFilterChart';
import TimelineChart from './components/timeline/TimelineChart';
import RadialWithBarChart from './components/radialwithbar/RadialWithBarChart'

class App extends Component {
  
  render() {

    let windowSize = window.innerWidth;
    return (
      <MuiThemeProvider muiTheme={theme}>
        <Router>
          <div>
            <AppBar
                //title={<div><img style={{height: "50px", width: "50px", marginTop: "10px", marginRight: "10px"}} src={require('./assets/cathay.jpg')}/>Cathay Financial Holdings</div>}
                title="Cathay Financial Holdings"
                showMenuIconButton={false}
              />
              <div className="App">
                <Tabs>
                  <Tab label={
                    <Link 
                      style={{color: 'white', textDecoration: 'none', paddingLeft: windowSize/10, paddingRight: windowSize/10, paddingTop: 20, paddingBottom: 20}} 
                      to="/bubbleswithfilter"> 消費行為意圖 
                    </Link>}>
                  </Tab>
                  <Tab label={
                    <Link 
                      style={{color: 'white', textDecoration: 'none', paddingLeft: windowSize/10, paddingRight: windowSize/10, paddingTop: 20, paddingBottom: 20}} 
                      to="/radialwithbar"> 行銷通路與客戶反應 
                    </Link>}>
                  </Tab>
                  <Tab label={
                    <Link style={{color: 'white', textDecoration: 'none', paddingLeft: windowSize/10, paddingRight: windowSize/10, paddingTop: 20, paddingBottom: 20}} 
                    to="/timeline"> 客戶最新十筆事件明細 
                    </Link>}>
                  </Tab>
                </Tabs>
              </div>
            <Route path='/bubbleswithfilter' component={BubbleWithFilterChart} />
            <Route path='/radialwithbar' component={RadialWithBarChart} />
            <Route path='/timeline' component={TimelineChart} />
          </div>
        </Router>
      </MuiThemeProvider>
    );
  }
}

export default App;
